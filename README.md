# QML syntax highlighling & snippets for VSCode - Visual Studio Code

## Part of [Cute IDE](https://www.cutetee.it) Project

- **Support development of Cute IDE on [Patreon](https://www.patreon.com/cutetee/overview).**
- **<a href="mailto:incoming+vxv/qml@incoming.gitlab.com?subject=QML">Contact Developers</a>**
- **Follow announcements on [Twitter](https://twitter.com/CuteteeI) or  [Reddit](https://www.reddit.com/user/cutetee_it)**
- **QML Snippets [Read the docs](https://doc.cutetee.it/qml)**


<img src="https://gitlab.com/vxv/qml/raw/master/images/but.gif" />

<img src="https://gitlab.com/vxv/qml/raw/master/images/monokai_dimmed_header.png" />

- [**Solarized Dark**](https://raw.githubusercontent.com/cutetee/qml/master/images/solarized_dark.png)
- [**Solarized Light**](https://github.com/cutetee/qml/raw/master/images/solarized.png)
- [**Monokai**](https://github.com/cutetee/qml/raw/master/images/monokai.png)
- [**Monokai Dimmed**](https://github.com/cutetee/qml/raw/master/images/monokai_dimmed.png)


<img width="280" src="https://raw.githubusercontent.com/cutetee/qml/master/images/solarized_dark.png" /> <img width="280" src="https://github.com/cutetee/qml/raw/master/images/solarized.png" /> <img width="280" src="https://github.com/cutetee/qml/raw/master/images/monokai.png" /> <img width="280" src="https://github.com/cutetee/qml/raw/master/images/monokai_dimmed.png" />