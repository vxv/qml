# Change Log
- https://keepachangelog.com/de/1.0.0/


## [1.3.0] - 2018-12-14
### Added
- Blockcomments 
/*
code
*/
### Fixed
- Doubleslash comments wrong color


## [1.2.1] - 2018-12-13
### Added
- Oneline Blockcomments like /* comment */


## [1.2.0] - 2018-12-10
### Added
- Snippets for basic types
- Snipptes List [but, butid, prop, pint, pbool, pdouble, pvar,pstr, palias, moa]


## [1.1.0] - 2018-12-6
### Change
- name from cutetee to qml
